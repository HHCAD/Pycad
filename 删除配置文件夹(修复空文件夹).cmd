:: /f 强制删除只读文件/s 从所有子目录删除指定文件/q 安静模式，删除时不要求确认
:: 递归删除指定文件和文件夹
rd /s /q  %~dp0temp
rd /s /q  %~dp0update
rd /s /q  %~dp0main

::递归删除所有后缀文件
del /a /f /s /q "*.keep"

pause